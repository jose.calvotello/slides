# Reveal.js Template for GitLab CI

This is a gerneic template for using GitLab CI together with Pages and Reveal.js.

## Usage

Copy or edit the files on `talks/generic-markdown`. Put your Slides content to
`slides.md`. Please also set the metadata (author name, title and the like) at
`index.html`.

Your slides will be available via GitLab pages, eg: https://mgoebel.pages.gwdg.de/slides/talks/generic-markdown/#/2 . 

## Credits

[Reveal.js](https://github.com/hakimel/reveal.js)


# Links
https://jose.calvotello.pages.gwdg.de/slides/talks/tutorial/
https://jose.calvotello.pages.gwdg.de/slides/talks/tutorial/?print-pdf
https://jose.calvotello.pages.gwdg.de/slides/talks/20210603_GOK/?print-pdf
