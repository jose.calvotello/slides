<!doctype html>
<html lang="en">


	<head>
		<meta charset="utf-8">
			
		<title>FAIR Derived Data in TEI for Copyrighted Texts</title>
			
		<meta name="description" content="FAIR Derived Data in TEI for Copyrighted Texts">
			<meta name="author" content="Dr. José Calvo Tello">

			<meta name="apple-mobile-web-app-capable" content="yes">
			<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
			
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			
			<link rel="stylesheet" href="../../lib/reveal/dist/reset.css">
			<link rel="stylesheet" href="../../lib/reveal/dist/reveal.css">
			<link rel="stylesheet" href="../../lib/reveal/dist/theme/white.css" id="theme">
			<link rel="stylesheet" href="custom.css">
			
			<!-- Theme used for syntax highlighting of code --> 	
			<link rel="stylesheet" href="../../lib/reveal/plugin/highlight/monokai.css" id="highlight-theme">
	</head>

	<body>

        <div class="reveal">
            <div class="slides">
              
				<section data-markdown="" data-separator="^\n---\n" data-separator-vertical="^\n--\n" data-charset="utf-8">
					<script type="text/template">
<!-- .slide: class="firstSlide" -->

## FAIR Derived Data in TEI for Copyrighted Texts

<br />

## TEI Conference 2023, Paderborn
### 06.09.2023

<hr />

#### José Calvo Tello<sup>1</sup>, Keli Du<sup>2</sup>, Mathias Göbel<sup>1</sup>, Nanette Rißler-Pipka<sup>3</sup>
##### SUB Göttingen (1), Trier University (3), Max Weber Stiftung (2)

---

# Derived Data, TEI and FAIR

--

## Derived Data, TEI and FAIR

* Copyrighted texts face restrictions on publication and re-use
* Removal of the primarily copyright-relevant features
* Extract derived data (see e.g. Lin et al. 2012, Bhattacharyya et al. 2015)
  * Specially important in some traditions (Sánchez Sánchez and Domínguez Cintas 2007, Calvo Tello und Rißler-Pipka 2023)
  * The HathiTrust Research Center Extracted Features Dataset (Jett et al. 2020)
  * *Abgeleitete Textformate* (Schöch et al. 2020)
  * German NFDI consortium Text+

--

<img src="./img/padme-anakin.jpg" width="600" />

--

## Derived Data, TEI and FAIR

* Ad-hoc solutions
* Deteriorates their FAIR status (Wilkinson et al. 2016)
* Type of data
  * **Text**
  * Textual structure
  * Metadata
  * Annotation


--

## Derived data in TEI
* Combination of different kind of data: text, metadata, structure, annotation
* Known and accepted in many communities (in contrast to TXT, JSON, MARC21-XML, CONNL, HTML, tsv, ePUB)
* TEI can be converted to these formats

--

## TEI and FAIR Principles

* Offers elements to identify (F1) and describe (F2) the document
* Is an open format (A1.1), data and metadata are in a single file (A2)
* Offers a rich vocabulary (I1) which follows FAIR criteria (I2), for the documentation of changes (I3)
* Decisions (R1), licenses (R1.1), and origin of the data (R1.2)


---

# Corpora

--

## Corpora
* **Gutenberg.de**: the German clone of “Project Gutenberg”, with mostly literary texts after copyright expires
* **American drama on CD-ROM**: a corpus of English plays (18th-20th centuries)
* **Spanish CoNSSA corpus**: partially published as TEI and tables (tsvs) with derived data (Calvo Tello 2021)
* **French corpus**: with 320 copyright protected novels (1980-1999)
* **Chinese corpus**: with 158 texts by Lu Xun (novels and essays, 1918-1936)
* One text per corpus

--

# Original Encoding

<img src="./img/image1.png" width="600" />

--

# Tokenized

<img src="./img/image2.png" width="100%" />


---

# Possibilities of Derived Data in TEI

--

## Bag of Words in TEI at Volume Level

<img src="./img/image3.png" width="80%" />


--

| Features    | BoW Volume |
| -------- | ------- |
| Metadata  | ✅    |
| Annotation | ✅     |
| Total frequency of tokens | ✅    |

--

* I want to know not only the frequency of the word *love* in the entire volume, but also how its frequency develops from the first to the last chapter or paragraph

--

## What about the development of the frequencies in divisions (chapters) or paragraphs?

--

## Bag of Words in TEI at Division (Chapter, Acts, Paragraphs) Level

<img src="./img/image4.png" width="80%" />

--

| Features    | BoW Volume | BoW Division
| -------- | ------- | ---- |
| Metadata  | ✅    | ✅ |
| Annotation | ✅     | ✅ |
| Total frequency    | ✅    | ✅ |
| Development frequency    | ❌    | ✅ | 

--

* I want to know also the number of verses, letters, italic passages, etc. in the file and chapters...

--

## What about the textual structure?

--

## Empty TEI Structure of the Text

<img src="./img/image5.png" width="80%" />

--

## In combination with

<img src="./img/image4.png" width="80%" />


--

| Features    | BoW Volume | BoW Division +  Textual Structure (without Text)
| -------- | ------- | ---- |
| Metadata  | ✅    | ✅ |
| Annotation | ✅     | ✅ |
| Total frequency    | ✅    | ✅ |
| Development frequency    | ❌    | ✅ | 
| Textual structure    | ❌    | ✅ | 

--

## Nice, but you are making my life harder, because now I have to extract the data from the measureGrp

--

## What about using standard tools?


--

## Randomized Order of Tokens

<img src="./img/image6.png" width="80%" />

--

| Features    | BoW Volume | BoW Division +  Textual Structure  | Random Order |
| -------- | ------- | ---- | --- |
| Metadata  | ✅    | ✅ | ✅ |
| Annotation | ✅     | ✅ | ✅ |
| Total frequency    | ✅    | ✅ | ✅ |
| Development frequency    | ❌    | ✅ | ✅ | 
| Textual structure    | ❌    | ✅ |  ✅ |
| Use of standard tools  | ❌    | ❌ |  ✅ |


--

## Lexical distributional information is central to many current methods

--

## What about calculating ngrams and collocations?

--

* Original text
  * *You may shut our lips, papa, but you can't close our hearts.*
* Form n-grams, for example 3-grams
  * *You_may_shut our_lips_, papa_,\_but you_can_' t_close_our hearts_.*
* Randomize the order of the n-grams while keeping the original order within the n-grams
  * *our_lips_, t_close_our hearts_. You_may_shut you_can_' papa_,_but*
* We do this, but with the TEI *seg* element

--

## N-Grams, then Randomize Order

<img src="./img/image7.png" width="80%" />

--

| Features    | BoW Volume | BoW Division +  Textual Structure | Random Order | N-Grams, Randomized |
| -------- | ------- | ---- | --- | --- |
| Metadata  | ✅    | ✅ | ✅ | ✅ | 
| Annotation | ✅     | ✅ | ✅ | ✅ |
| Total frequency    | ✅    | ✅ | ✅ | ✅ |
| Development frequency    | ❌    | ✅ | ✅ |  ✅ |
| Textual structure    | ❌    | ✅ |  ✅ | ✅ |
| Use of standard tools  | ❌    | ❌ |  ✅ | ✅ |
| Ngrams and collocations  | ❌    | ❌ |  ❌ | ✅ |


---

# Conclusions

--

## Conclusions
* Text is only one type of data
* Text faces more restriction about its publication
* Metadata, structure, annotation are also important and have fewer restriction in its publication
* Considering the FAIR criteria, TEI is good format candidate to publish derived data


--

## Our current suggestion
* N-grams in randomized order within block elements (*p, ab, l*) in TEI
<img src="./img/image7_1.jpg" width="70%" />

--

## Problem solved?

--

| Features    | BoW Volume | BoW Division +  Textual Structure | Random Order | N-Grams, Randomized |
| -------- | ------- | ---- | --- | --- |
| Metadata  | ✅    | ✅ | ✅ | ✅ | 
| Annotation | ✅     | ✅ | ✅ | ✅ |
| Total frequency    | ✅    | ✅ | ✅ | ✅ |
| Development frequency    | ❌    | ✅ | ✅ |  ✅ |
| Textual structure    | ❌    | ✅ |  ✅ | ✅ |
| Use of standard tools  | ❌    | ❌ |  ✅ | ✅ |
| Ngrams and collocations  | ❌    | ❌ |  ❌ | ✅ |
| Other analysis  | ❌    | ❌ |  ❌ | ❌ |

--

# Open Questions
* Is this what the community wants and needs?
* Are we spoiling other important information without realizing it?
* What text level should be used: div, paragraph, sentence?
* How do we inform users that the published file with derived data is something **different** from the original file?

--

## Digitization
* Many cultural heritage institutions do not use TEI in their digitization
* Solution for editorial and research projects encoding in TEI

--

## Possible new feature in TextGrid Repository?
* New module in TextGrid Repository for the extraction and publication of derived data in TEI?
  * Researcher brings TEI files that cannot be simply published
  * TextGrid creates new versions of the files with derived data in TEI
  * Researcher can publish these derived data in TEI files in TextGrid Repository

--

## Do you have copyrighted texts in TEI and want to publish them in TextGrid Repository?
* Talk to us!
* We are looking for use-cases and partners!

--

# Oh, and by the way...

--

## Further Activities in the NFDI Consortium Text+ 
### Survey: What metadata do literary studies need?
* What is the community using as categories when creating corpora?
* Do the resources (catalogs, authority files, repositories) have this metadata?
* If not, what can be done to improve the situation?
* https://survey.academiccloud.de/index.php/916873?lang=en
* DHd Blog: https://dhd-blog.org/?p=19871
* TEI Mailing-List (August 14th, 2023)
* It only takes around 10 minutes!

<img src="./../../images/logos-survey.png" />

--

<img src="./../../images/thanks.jpg" />


					</script>
				</section>
            </div>
        </div>

        <script src="../../lib/reveal/dist/reveal.js"></script>
        <script src="../../lib/reveal/plugin/zoom/zoom.js"></script>
        <script src="../../lib/reveal/plugin/notes/notes.js"></script>
        <script src="../../lib/reveal/plugin/search/search.js"></script>
        <script src="../../lib/reveal/plugin/markdown/markdown.js"></script>
        <script src="../../lib/reveal/plugin/highlight/highlight.js"></script>
        <script>
    
          // Also available as an ES module, see:
          // https://revealjs.com/initialization/
          Reveal.initialize({
            controls: true,
            progress: true,
            center: true,
            hash: true,
    
            // Learn about plugins: https://revealjs.com/plugins/
            plugins: [ RevealZoom, RevealNotes, RevealSearch, RevealMarkdown, RevealHighlight ]
          });
    
        </script>
    
      </body>
    </html>
    