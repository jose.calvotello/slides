## Using Jupyter Notebooks for the Reproducibility of a Digital Humanities PhD
José Calvo Tello, @twitter

<small>Goettingen State and University Library</small>

<!--

### Using Jupyter Notebooks for the Reproducibility of a Digital Humanities PhD
## ToC

* The context of my PhD
* Why did I use Jupyter Notebooks?
* How did I use them?
* What did work well?
* What could have worked better?
-->

---

## The context of my PhD

* Digital Humanities (literature, linguistics)
* Machine Learning applied to Literary Genres
* Analyzed 358 Spanish novels 
* Some still under copyright
* XML, TEI, Python, Linked Open Data, Git...
* 400 pages (too long, I know...)
* Over 5 years
* Writing for the last 2 years

--

## Viva!


--

## Learn from my Mistakes!

---

## Why did I use Jupyter Notebooks?

- Long period
- Many chapters
- Review process 
- Motivation:
    1. Own documentation
    2. Reproducibility 

---

# How did I use them?

--

## Git Repositories

1. **Data**: texts, annotation, metadata...
2. **General scripts**: Machine Learning, visualization, statistical tests, transformation of data...
3. **Jupyter Notebooks**: for PhD


--

## Jupyter Notebooks
- For each chapter, one companion Notebook
- Brief descriptions of sections

--

## Jupyter Notebooks
- Notebooks are structured with the same headers as in the chapter


--

## What did work well?

- Very good documentation 
- Refer in the thesis to the Notebook for further details
- Test further possibilities without mentioning in the thesis
- Easier review process
- Manage illustrations

--

# What could have worked better?

- Not fully replicable
- Paths between three repositories
- Availability of the data
- Evolving learning skills and code
- No institutional support
- Better integreation between output (data, illustrations) and prose of the thesis

--

# Tensions between Values!
## Which Aspects are Critical?

- Replicability
- Data curation
- Programming
- Code sharing within a reserach group
- Copyright
- Innovation

--

## What are the Best Practices going to be in 2025?


---

![an image](img/some-image.png)

you can insert Images…

[…and Hyperlinks](https://de.dariah.eu/)

--

![from wikicommons](https://upload.wikimedia.org/wikipedia/commons/8/8b/Campsite_at_Mystic_Beach%2C_Vancouver_Island%2C_Canada.jpg)


--

## Headline 2

- Lists
  - sublists
  - yes
- another point

--

### Headline 3

Some awesome text here.

--

<!-- .slide: data-background-color="lightblue" -->

## Background Color Makes Life Good

--

<!-- .slide: data-background-image="img/some-image.png" -->
![a single transparent pixel](img/pixel.png) <!-- .element height="100%" width="100%" -->

### Background Image

---

What about a table? You can easily create one with the help of [this tool](https://www.tablesgenerator.com/markdown_tables).

| Head 1 | Head 2 | Head 3 |
|----------|--------|--------|
| centered | left | right |
| content | align | align |
